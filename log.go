package ginplus

import (
	"gitee.com/hongzhaomin/hzm-common-go/easylog"
	"log/slog"
)

var log = easylog.NewDefaultLogger(slog.LevelInfo)
