package ginplus

import "reflect"

// 处理器调用链，以controller为维度
type handlerMappingChain struct {
	controller IController
	mappings   []*handlerMapping
}

type handlerMapping struct {
	url         string         // 处理器映射url
	method      string         // 请求方式
	fun         handlerFun     // 处理器函数
	permissions []string       // 权限值，从字段TAG(Permission)中解析出来的值列表
	ctrlType    ControllerType // 控制器类型
}

type handlerFun struct {
	rvFun     reflect.Value
	funParams []handlerFunParam
}

type handlerFunParam struct {
	definedSpecialParamType DefinedSpecialParamTypeEnum // 定义的特殊参数类型
	definedParamIsPtr       bool                        // 定义的参数是否为指针类型：true -> 是；false -> 否
	rtParam                 reflect.Type                // 参数的 reflect.Type 值
	pName                   string                      // 参数名称
	defaultValMap           map[string]string           // 参数字段默认值（只支持结构体参数），使用default TAG声明
}
