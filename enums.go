package ginplus

import "gitee.com/hongzhaomin/hzm-common-go/strutil"

// ControllerType 枚举
/********************************************************/
/************* 开发者继承的controller类型 ******************/
/********************************************************/
type ControllerType uint

func (ct ControllerType) String() string {
	switch ct {
	case controller:
		return fullPathCtrlName
	case restController:
		return fullPathRestCtrlName
	default:
		return strutil.Empty
	}
}

func (ct ControllerType) is(ctrlType ControllerType) bool {
	return ct == ctrlType
}

func getCtrlType(fullPathName string) ControllerType {
	switch fullPathName {
	case fullPathCtrlName:
		return controller
	case fullPathRestCtrlName:
		return restController
	default:
		return unknown
	}
}

const (
	unknown        ControllerType = iota // 未知
	controller                           // Controller
	restController                       // RestController
)

// DefinedSpecialParamTypeEnum 枚举
/********************************************************/
/*********** 开发者定义的映射函数特殊的参数类型枚举 *************/
/********************************************************/
type DefinedSpecialParamTypeEnum uint

func (dspt DefinedSpecialParamTypeEnum) String() string {
	switch dspt {
	case ginCtx:
		return fullPathGinCtxName
	case mulFile:
		return fullPathMulFileName
	case mulForm:
		return fullPathMulFormName
	default:
		return strutil.Empty
	}
}

func (dspt DefinedSpecialParamTypeEnum) is(dspType DefinedSpecialParamTypeEnum) bool {
	return dspt == dspType
}

func getDefinedSpecialParamType(fullPathName string) DefinedSpecialParamTypeEnum {
	switch fullPathName {
	case fullPathGinCtxName:
		return ginCtx
	case fullPathMulFileName:
		return mulFile
	case fullPathMulFormName:
		return mulForm
	default:
		return omit
	}
}

const (
	omit    DefinedSpecialParamTypeEnum = iota // 忽略
	ginCtx                                     // gin.Context
	mulFile                                    // multipart.FileHeader
	mulForm                                    // multipart.Form
)
