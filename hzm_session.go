package ginplus

import (
	"gitee.com/hongzhaomin/hzm-common-go/strutil"
	"time"
)

type session struct {
	userJson     string // 用户json
	cacheTime    int64  // 缓存时间
	survivalTime int64  // 存活时间
	accessToken  string // token
}

func cleanSessionTask(t *time.Ticker) {
	for {
		select {
		case <-t.C:
			expiredTokens := make([]string, 0)
			for token, sess := range token2SessionMap {
				if sessionExpired(sess) {
					expiredTokens = append(expiredTokens, token)
				}
			}
			for _, token := range expiredTokens {
				log.Info("token已过期: %s，即将删除", token)
				delete(token2SessionMap, token)
			}
		}
	}
}

func sessionExpired(sess *session) bool {
	return sess.cacheTime+sess.survivalTime < time.Now().UnixMilli()
}

func init() {
	ticker := time.NewTicker(24 * time.Hour)
	go cleanSessionTask(ticker)
}

var (
	token2SessionMap   = make(map[string] /*token*/ *session /*session*/)
	sessionCh          = make(chan *session /*session*/, 5)
	currentGid2UserMap = make(map[string] /*gid*/ string /*userJson*/)
	gidCh              = make(chan []string /*[gid,userJson]*/, 5)
)

// ============================登录用户和当前协程绑定==============================

func GetCurrentUserJson() string {
	return currentGid2UserMap[strutil.GetCurrentGid()]
}

func setCurrentUserJson(userJson string) {
	if strutil.IsNotBlank(userJson) {
		gidCh <- []string{strutil.GetCurrentGid(), userJson}
		go opeGidChan(gidCh, currentGid2UserMap)
	}
}

func removeCurrentUserJson() {
	gidCh <- []string{strutil.GetCurrentGid(), strutil.Empty}
	go opeGidChan(gidCh, currentGid2UserMap)
}

// ============================登录用户session管理==============================

func GetUserJsonByToken(token string) string {
	sess := token2SessionMap[token]
	// 过期则懒删除
	if sess == nil || sessionExpired(sess) {
		DeleteSession(token)
		return strutil.Empty
	}
	return sess.userJson
}

func CacheUserJson2Session(token string, userJson string) {
	if strutil.IsNotBlank(token) && strutil.IsNotBlank(userJson) {
		sess := new(session)
		sess.userJson = userJson
		sess.accessToken = token
		sess.cacheTime = time.Now().UnixMilli()
		// 24小时后过期
		sess.survivalTime = 24 * time.Hour.Milliseconds()
		sessionCh <- sess
		go func() {
			se := <-sessionCh
			token2SessionMap[se.accessToken] = se
		}()
	}
}

func DeleteSession(token string) {
	if strutil.IsNotBlank(token) {
		sess := token2SessionMap[token]
		if sess != nil {
			sessionCh <- sess
			go func() {
				se := <-sessionCh
				delete(token2SessionMap, se.accessToken)
				log.Info("token过期清理完成: %s", token)
			}()
		}
	}
}

func opeGidChan(ch <-chan []string, ma map[string]string) {
	key2JsonSlice := <-ch
	if strutil.IsBlank(key2JsonSlice[1]) {
		delete(ma, key2JsonSlice[0])
		return
	}
	ma[key2JsonSlice[0]] = key2JsonSlice[1]
}
