package ginplus

import (
	"fmt"
	"reflect"
	"testing"
)

func TestGinplus(t *testing.T) {
	c := new(Controller)
	rvCtrl := reflect.ValueOf(c)

	//rtPrt := reflect.New(rvCtrl.Type())

	implements := rvCtrl.Type().Implements(IControllerType)
	fmt.Println(implements)
	implements = rvCtrl.Elem().Type().Implements(IControllerType)
	fmt.Println(implements)
}
