package ginplus

import (
	"html/template"
	"io"
	"net/http"
	"time"
)

type errPage struct {
	TimeFormat string
	ErrType    string
	ErrStatus  int
}

var errPageTemplate = `<!DOCTYPE html>
<html lang="en">
<head></head>
<body>
	<h1>Whitelabel Error Page</h1>
    <p>This application has no explicit mapping for /error, so you are seeing this as a fallback.</p>
	<div id="created">{{.TimeFormat}}</div>
	<div>There was an unexpected error (type={{.ErrType}}, status={{.ErrStatus}}).</div>
</body>
</html>`

func printErrPage(w io.Writer, status int) {
	// 解析指定文件生成模板对象
	tmpl, err := template.New("404").Parse(errPageTemplate)
	if err != nil {
		panic(err)
	}
	// 利用给定数据渲染模板，并将结果写入w
	data := errPage{
		TimeFormat: time.Now().Format(time.UnixDate),
		ErrType:    http.StatusText(status),
		ErrStatus:  status,
	}
	if err = tmpl.Execute(w, data); err != nil {
		panic(err)
	}
}
