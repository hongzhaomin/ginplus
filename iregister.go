package ginplus

import "github.com/gin-gonic/gin"

// IGinConfiguration 接口，要使用ginplus必须实现该接口
type IGinConfiguration interface {

	// ContextPath 项目路径配置
	ContextPath() string

	// ServerPort 服务端口配置
	ServerPort() string

	// CtrlRegisPreProcessorFunc 控制器注册前处理器方法
	CtrlRegisPreProcessorFunc(*gin.Engine)

	// RegisControllers 注册控制器
	RegisControllers() []IController

	// UseBuiltinRespStruct 是否使用内置响应结构体
	UseBuiltinRespStruct() bool

	// GlobalPanicHandler 配置全局异常处理
	GlobalPanicHandler(err any) any
}

var _ IGinConfiguration = (*DefaultGinConfiguration)(nil)

type DefaultGinConfiguration struct {
}

func (dgc *DefaultGinConfiguration) ContextPath() string {
	return "/"
}

func (dgc *DefaultGinConfiguration) ServerPort() string {
	return "8080"
}

func (dgc *DefaultGinConfiguration) CtrlRegisPreProcessorFunc(*gin.Engine) {
	// nothing to do
}

func (dgc *DefaultGinConfiguration) RegisControllers() []IController {
	return nil
}

func (dgc *DefaultGinConfiguration) UseBuiltinRespStruct() bool {
	return true
}

func (dgc *DefaultGinConfiguration) GlobalPanicHandler(err any) any {
	return nil
}
