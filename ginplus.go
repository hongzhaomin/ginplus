package ginplus

import (
	"gitee.com/hongzhaomin/hzm-common-go/strutil"
	"github.com/gin-gonic/gin"
	"os"
	"reflect"
	"runtime"
)

const (
	rootPath     = "/"            // 根路径
	AccessToken  = "accessToken"  // 请求token变量名
	RequireLogin = "RequireLogin" // Tag的value项，Permission的固定选项值
	ginTagForm   = "form"         // gin表单解析tag名称
	ginTagUri    = "uri"          // gin路径参数解析tag名称
)

var ginConfig IGinConfiguration = nil

// StartHttpServerDefault http服务启动入口
func StartHttpServerDefault() {
	StartHttpServer(new(DefaultGinConfiguration))
}

func StartHttpServer(config IGinConfiguration) {
	hostName, _ := os.Hostname()
	log.Info("Starting http server using %s on %s", runtime.Version(), hostName)
	// 创建gin处理器注册器对象
	ginConfig = config
	// 创建路由对象
	router := gin.Default()
	// 控制器注册前置处理：例如：加载模板、配置静态web目录、自定义校验器等操作
	ginConfig.CtrlRegisPreProcessorFunc(router)
	//router.LoadHTMLGlob("templates/*")
	//router.Static("/static", "./static")
	//if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
	//	v.RegisterValidation("bookabledate", bookableDate)
	//}
	// 注册处理器
	//group(router, ginConfig)
	webCtx := NewWebContext(router, ginConfig)
	webCtx.StartHttpServer()
}

//func group(r *gin.Engine, register IGinConfiguration) {
//	// 主分组
//	root := r.Group(register.ContextPath())
//
//	nextGroup(root, register)
//}

//func nextGroup(root *gin.RouterGroup, register IGinConfiguration) {
//	log.Info("Revolve controllers starting...")
//	resolver := ControllerResolver{
//		ctrlList: register.RegisControllers(),
//	}
//	mappingChains := resolver.resolveCtrlList()
//	log.Info("Completed Revolve controllers")
//	url2Mapping = make(map[string]*handlerMapping)
//	for _, mappingChain := range mappingChains {
//		controller := mappingChain.controller
//		controllerPath := controller.RequestMapping()
//		group := root.Group(controllerPath)
//		mappings := mappingChain.mappings
//		for _, mapping := range mappings {
//			basePath := strutil.JoinPaths(rootPath, register.ContextPath())
//			fullPath := strutil.JoinPaths(strutil.JoinPaths(basePath, controllerPath), mapping.url)
//			putHandlerMapping(fullPath, mapping)
//			httpRegis(group, mapping)
//		}
//	}
//	log.Info("Completed registered %d controllers", len(mappingChains))
//}

//func httpRegis(group *gin.RouterGroup, mapping *handlerMapping) {
//	url := mapping.url
//	method := mapping.method
//	switch method {
//	case http.MethodGet:
//		group.GET(url, commonHandlerFun)
//	case http.MethodPost:
//		group.POST(url, commonHandlerFun)
//	case http.MethodPut:
//		group.PUT(url, commonHandlerFun)
//	case http.MethodDelete:
//		group.DELETE(url, commonHandlerFun)
//	default:
//		log.Error("暂不支持http请求：%s，请求url为：%s", method, url)
//		panic(errors.New("不支持的http请求"))
//	}
//}

//func defaultGlobalPanicHandler(ctx *gin.Context) {
//	var err = recover()
//	if err != nil {
//		log.Error("[%s]请求异常，%v", err)
//		switch err.(type) {
//		case *bizError:
//			bizErr := err.(*bizError)
//			ctx.JSON(http.StatusOK, Fail2(bizErr.Code, bizErr.Msg))
//		case string:
//			ctx.JSON(http.StatusOK, Fail(err.(string)))
//		default:
//			ctx.JSON(http.StatusOK, Fail("服务开小差了"))
//		}
//	}
//}

//func customerGlobalPanicHandler(ctx *gin.Context) {
//	var err = recover()
//	if err != nil {
//		errData := ginConfig.GlobalPanicHandler(err)
//		if errData == nil {
//			// 返回为nil, 表示未开启全局异常处理配置，继续将错误抛出
//			panic(err)
//		}
//		ctx.JSON(http.StatusOK, errData)
//	}
//}

//func commonHandlerFun(ctx *gin.Context) {
//	// 全局异常处理
//	if ginConfig.UseBuiltinRespStruct() {
//		// 默认全局异常处理
//		defer defaultGlobalPanicHandler(ctx)
//	} else {
//		// 自定义全局异常处理
//		defer customerGlobalPanicHandler(ctx)
//	}
//
//	// 根据路径获取处理器
//	mapping := getHandlerMapping(ctx.FullPath())
//	if mapping == nil {
//		ctx.JSON(http.StatusNotFound, nil)
//		return
//	}
//
//	// 准备参数
//	rvParams, existCtx := prepareParams(mapping.fun, ctx)
//
//	// todo 可扩展处理器调用前逻辑
//	if ok := invokePre(ctx, mapping.permissions); !ok {
//		return
//	}
//
//	// 调用处理器函数
//	data := doInvoke(mapping.fun.rvFun, rvParams)
//
//	// todo 可扩展处理器调用后逻辑
//	removeCurrentUserJson()
//
//	// 响应处理
//	switch mapping.ctrlType {
//	case controller:
//		responseHtml(data, existCtx, ctx)
//	case restController:
//		responseJson(data, existCtx, ctx, mapping.fun)
//	default:
//		responseJson(data, existCtx, ctx, mapping.fun)
//	}
//}

//func prepareParams(fun handlerFun, ctx *gin.Context) ([]reflect.Value, bool) {
//	// 构造方法入参列表
//	existCtx := false
//	rvParams := make([]reflect.Value, 0)
//	for _, funParam := range fun.funParams {
//		paramName := funParam.pName
//		// 判断是否为特殊类型参数
//		switch funParam.definedSpecialParamType {
//		case ginCtx:
//			rvParams = append(rvParams, reflect.ValueOf(ctx))
//			existCtx = true
//			continue
//		case mulFile:
//			if strutil.IsBlank(paramName) {
//				paramName = "file"
//			}
//			file, err := ctx.FormFile(paramName)
//			if err != nil {
//				log.Error("单文件参数绑定失败，%v", err)
//				panic(errors.New("单文件参数绑定失败"))
//			}
//			rvParams = append(rvParams, reflect.ValueOf(file))
//			continue
//		case mulForm:
//			form, err := ctx.MultipartForm()
//			if err != nil {
//				log.Error("多文件参数绑定失败，%v", err)
//				panic(errors.New("多文件参数绑定失败"))
//			}
//			rvParams = append(rvParams, reflect.ValueOf(form))
//			continue
//		}
//
//		// 创建实例
//		var rvParam reflect.Value
//		rtParam := funParam.rtParam
//		defaultValMap := funParam.defaultValMap
//
//		switch rtParam.Kind() {
//		case reflect.Struct:
//			// 创建结构体实例，拿到指针
//			rvParam = reflect.New(rtParam)
//			// form请求参数方式并且不存在form TAG
//			form := strings.Contains(binding.Default(ctx.Request.Method, ctx.ContentType()).Name(), ginTagForm)
//			if form && toolkit.NotExistTag(rtParam, ginTagForm) {
//				// 构建新结构体，然后将数据绑定到新的结构体上
//				rtParamNew := copyStructFields(rtParam, strutil.Empty, defaultValMap)
//				rvParamNew := reflect.New(rtParamNew)
//				if err := ctx.ShouldBind(rvParamNew.Interface()); err != nil {
//					log.Error("数据绑定失败，%v", err)
//					panic(errors.New("数据绑定失败"))
//				}
//				// 将新结构体值的指针转化为 rtParam 结构体类型的指针
//				rvParam = reflect.NewAt(rtParam, unsafe.Pointer(rvParamNew.Pointer()))
//				// 将新结构体转成json，再将json转成原始结构体
//				// 从而达到将数据绑定到原始结构体上的目的
//				//toJson := toolkit.ToJson3(rvParamNew.Interface())
//				//toolkit.ToObj(toJson, rvParam.Interface())
//			} else {
//				// 做数据绑定
//				if err := ctx.ShouldBind(rvParam.Interface()); err != nil {
//					log.Error("数据绑定失败，%v", err)
//					panic(errors.New("数据绑定失败"))
//				}
//				setDefaultVal(rvParam.Elem(), strutil.Empty, defaultValMap)
//			}
//			// 尝试绑定路径参数
//			tryToBindUri(ctx, rvParam.Interface())
//			// 统一转化为指针指向的值
//			rvParam = rvParam.Elem()
//		case reflect.Map:
//			// 创建 map 体实例
//			rvParam = reflect.MakeMap(rtParam)
//			// 尝试绑定路径参数
//			tryToBindUri(ctx, rvParam.Interface())
//			// 做数据绑定
//			if err := ctx.ShouldBind(rvParam.Interface()); err != nil {
//				log.Error("数据绑定失败，%v", err)
//				panic(errors.New("数据绑定失败"))
//			}
//		case reflect.Slice:
//			if strutil.IsBlank(paramName) {
//				// 如果未指定参数名，则认为请求为body方式，使用shouldBind方式绑定数据
//				paramTemp := reflect.MakeSlice(rtParam, 0, 0).Interface()
//				// 创建临时切片绑定数据，类型为参数定义的类型
//				if err := ctx.ShouldBind(&paramTemp); err != nil {
//					log.Error("数据绑定失败，%v", err)
//					panic(errors.New("数据绑定失败"))
//				}
//				// 再利用临时切片的数据进行反射，转换为rtParam类型的切片数据
//				rvParamTemp := reflect.ValueOf(paramTemp)
//				// 不知道为什么? 这里rvParamTemp类型并不是定义的类型，那么我们创建定义的切片类型进行数据转换
//				rvParam = reflect.MakeSlice(rtParam, 0, rvParamTemp.Len())
//				// 反射方式遍历元素
//				for i := 0; i < rvParamTemp.Len(); i++ {
//					// 获取下标元素值的 reflect.Value
//					tempEle := rvParamTemp.Index(i)
//					convertPtr := basicConvert4Interface(rtParam.Elem(), tempEle.Interface())
//					// 解析阶段已经限制了切片元素类型不能为指针类型
//					rvParam = reflect.Append(rvParam, convertPtr.Elem())
//				}
//				break
//			}
//			sliceStr := ctx.Query(paramName)
//			sliceVal := make([]string, 0)
//			err := json.Unmarshal([]byte(sliceStr), &sliceVal)
//			if err != nil {
//				sliceVal = strutil.SpitNotLetterAndNumber(sliceStr)
//			}
//			// 根据 rtParam 创建一个切片
//			rvParam = reflect.MakeSlice(rtParam, 0, len(sliceVal))
//			for _, ele := range sliceVal {
//				rvParam = reflect.Append(rvParam, basicConvert(rtParam.Elem(), ele).Elem())
//			}
//		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
//			reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
//			reflect.Float32, reflect.Float64, reflect.String, reflect.Bool:
//			if strutil.IsBlank(paramName) {
//				log.Error("绑定参数失败：参数名[%s]为空", fun.rvFun.Type().Name())
//				panic(errors.New("绑定参数失败：参数名为空"))
//			}
//			var val string
//			switch {
//			case strings.Contains(ctx.FullPath(), strutil.Colon+paramName):
//				// 该参数名paramName为路径参数
//				val = ctx.Param(paramName)
//			case ctx.Request.Method != http.MethodGet:
//				// 非get请求，先去表单获取，未获取到再取query
//				val = ctx.PostForm(paramName)
//				if strutil.IsBlank(val) {
//					val = ctx.Query(paramName)
//				}
//			default:
//				// 其他当成query参数取
//				val = ctx.Query(paramName)
//			}
//			// 统一转化为指针指向的值
//			rvParam = basicConvert(rtParam, val).Elem()
//		default:
//			msg := fmt.Sprintf("不支持%s类型参数的解析", rtParam.Kind())
//			panic(msg)
//		}
//		// 方法入参填值
//		if funParam.definedParamIsPtr {
//			rvParams = append(rvParams, rvParam.Addr())
//		} else {
//			rvParams = append(rvParams, rvParam)
//		}
//	}
//	return rvParams, existCtx
//}

//func tryToBindUri(ctx *gin.Context, paramPtr any) {
//	if strings.Contains(ctx.FullPath(), strutil.Colon) {
//		// 路径包含冒号，表示路径参数
//		rv := reflect.Indirect(reflect.ValueOf(paramPtr))
//		rt := rv.Type()
//		if rv.Kind() == reflect.Struct && toolkit.NotExistTag(rt, ginTagUri) {
//			// 获取所有路径参数名称
//			pNames := streams.OfSlice(strings.Split(ctx.FullPath(), strutil.Slash)).
//				Filter(func(t string) bool {
//					return strings.Contains(t, strutil.Colon)
//				}).
//				MapIdentity(func(t string) string {
//					return strings.ReplaceAll(t, strutil.Colon, strutil.Empty)
//				}).ToSlice()
//			pMap := make(map[string]string)
//			if err := ctx.ShouldBindUri(pMap); err != nil {
//				log.Error("路径参数绑定失败，%v", err)
//				panic(errors.New("路径参数绑定失败"))
//			}
//			for i := 0; i < rt.NumField(); i++ {
//				structField := rt.Field(i)
//				contains, pName := slice.ContainsIgnoreCaseReturnEle(pNames, structField.Name)
//				if contains {
//					rvField := rv.Field(i)
//					rvField.Set(basicConvert(rvField.Type(), pMap[pName]).Elem())
//				}
//			}
//			return
//		}
//		if err := ctx.ShouldBindUri(paramPtr); err != nil {
//			log.Error("路径参数绑定失败，%v", err)
//			panic(errors.New("路径参数绑定失败"))
//		}
//	}
//}

//func copyStructFields(rtStruct reflect.Type, fieldName string, defaultValMap map[string]string) reflect.Type {
//	fieldNum := rtStruct.NumField()
//	newStructFields := make([]reflect.StructField, 0, fieldNum)
//	for i := 0; i < fieldNum; i++ {
//		field := rtStruct.Field(i)
//		if strutil.IsNotBlank(field.PkgPath) {
//			// 字段不可导出
//			continue
//		}
//		var key string
//		if strutil.IsNotBlank(fieldName) {
//			key = fieldName + strutil.Dot + field.Name
//		} else {
//			key = field.Name
//		}
//
//		rtField := field.Type
//		if rtField.Kind() == reflect.Ptr {
//			rtField = rtField.Elem()
//		}
//		if rtField.Kind() == reflect.Struct && field.Anonymous {
//			// 内嵌结构体，不加tag
//			rtEmbeddedStruct := copyStructFields(rtField, field.Name, defaultValMap)
//			field.Type = rtEmbeddedStruct
//		} else {
//			if rtField.Kind() == reflect.Struct {
//				// 非内嵌结构体，加tag，同时也要递归
//				if strutil.IsBlank(getDefinedSpecialParamType(getFullPathTypeName(rtField)).String()) {
//					// 但注意：特殊结构体（如文件类型的结构体）除外，因为我改变了其结构体，gin为该类型赋值时会报错
//					rtNotEmbeddedStruct := copyStructFields(rtField, field.Name, defaultValMap)
//					field.Type = rtNotEmbeddedStruct
//				}
//			}
//			tag := field.Tag
//			// 如果存在uri(TAG)，就不添加form(TAG)了
//			_, existUri := tag.Lookup(ginTagUri)
//			if _, ok := tag.Lookup(ginTagForm); !ok && !existUri {
//				// `form:"form,default=1"`
//				sb := new(strings.Builder)
//				sb.WriteString(string(tag))
//				fieldNameHump := strutil.FirstLetter2Lower(field.Name)
//				if sb.Len() > 0 {
//					sb.WriteString(strutil.Space)
//				}
//				sb.WriteString(ginTagForm)
//				sb.WriteString(strutil.Colon)
//				sb.WriteString(strutil.Quote)
//				sb.WriteString(fieldNameHump)
//				if strutil.IsNotBlank(defaultValMap[key]) {
//					sb.WriteString(",default=")
//					sb.WriteString(defaultValMap[key])
//				}
//				sb.WriteString(strutil.Quote)
//				field.Tag = reflect.StructTag(sb.String())
//			}
//		}
//		newStructFields = append(newStructFields, field)
//	}
//	return reflect.StructOf(newStructFields)
//}

//func setDefaultVal(rvParam reflect.Value, fieldName string, defaultValMap map[string]string) {
//	rtParam := rvParam.Type()
//	for i := 0; i < rvParam.NumField(); i++ {
//		structField := rtParam.Field(i)
//		if strutil.IsNotBlank(structField.PkgPath) {
//			// 字段不可导出
//			continue
//		}
//		var key string
//		if strutil.IsNotBlank(fieldName) {
//			key = fieldName + strutil.Dot + structField.Name
//		} else {
//			key = structField.Name
//		}
//
//		isPtr := false
//		rtField := structField.Type
//		if rtField.Kind() == reflect.Ptr {
//			rtField = rtField.Elem()
//			isPtr = true
//		}
//		rvField := rvParam.Field(i)
//		if rvField.IsZero() {
//			if rtField.Kind() == reflect.Struct {
//				rvFieldNew := reflect.New(rtField)
//				setDefaultVal(rvFieldNew.Elem(), structField.Name, defaultValMap)
//				if isPtr {
//					rvField.Set(rvFieldNew)
//				} else {
//					rvField.Set(rvFieldNew.Elem())
//				}
//			} else {
//				// 如果是零值，则赋默认值
//				if strutil.IsNotBlank(defaultValMap[key]) {
//					structFieldVal := basicConvert(rvField.Type(), defaultValMap[key])
//					if isPtr {
//						rvField.Set(structFieldVal)
//					} else {
//						rvField.Set(structFieldVal.Elem())
//					}
//				}
//			}
//		}
//	}
//}

// 执行方法
//func doInvoke(rvFun reflect.Value, rvParams []reflect.Value) any {
//	// 执行方法
//	results := rvFun.Call(rvParams)
//	if len(results) > 0 {
//		// 返回第一个响应的真实类型
//		// fixme 当方法返回值定义为指针类型时，返回结果为nil，此时的nil并非我们理解的nil，用 val == nil 为false
//		val := results[0]
//		if val.IsValid() {
//			// 判断返回是否为零值，如果是指针里面会判断是否为nil，不为零值则取真实值
//			if !val.IsZero() {
//				return val.Interface()
//			}
//		}
//	}
//	return nil
//}

var builtinResultTypeFullName = getFullPathTypeName(reflect.TypeOf(Result[any]{}))

//func responseJson(data any, existCtx bool, ctx *gin.Context, fun handlerFun) {
//	// 如果开发者使用自定义的响应结构体，则框架不做任何默认返回，直接返回响应结果
//	if !ginConfig.UseBuiltinRespStruct() {
//		ctx.JSON(http.StatusOK, data)
//		return
//	}
//	rtFun := fun.rvFun.Type()
//	// 响应结果个数为0，也没有使用ctx参数
//	if rtFun.NumOut() == 0 && !existCtx {
//		ctx.JSON(http.StatusOK, Ok())
//		return
//	}
//	// 存在响应结果
//	if data != nil {
//		if strutil.Equals(builtinResultTypeFullName, getFullPathTypeName(rtFun.Out(0))) {
//			ctx.JSON(http.StatusOK, data)
//			return
//		}
//		ctx.JSON(http.StatusOK, Ok2(data))
//	} else {
//		ctx.JSON(http.StatusOK, Ok())
//	}
//}

//func responseHtml(data any, existCtx bool, ctx *gin.Context) {
//	if existCtx {
//		return
//	}
//	switch modelAndView := data.(type) {
//	case string:
//		ctx.HTML(http.StatusOK, modelAndView, nil)
//	case ModelAndView:
//		ctx.HTML(modelAndView.HttpStatus, modelAndView.View, modelAndView.Model)
//	case *ModelAndView:
//		ctx.HTML(modelAndView.HttpStatus, modelAndView.View, modelAndView.Model)
//	default:
//		log.Error("无法解析的model and view")
//		panic(errors.New("无法解析的model and view"))
//	}
//}

func GetAccessToken(ctx *gin.Context) string {
	// 先从路径中获取token
	token := ctx.Query(AccessToken)
	if strutil.IsBlank(token) {
		// 未获取到，再从header中获取
		token = ctx.GetHeader(AccessToken)
	}
	if strutil.IsBlank(token) {
		// 还未获取到，最后从cookie中获取
		if cookie, err := ctx.Cookie(AccessToken); err == nil {
			// fixme 这个从cookie中获取的方式有待测试
			token = cookie
		}
	}
	return token
}

//func invokePre(ctx *gin.Context, permissions []string) bool {
//	removeCurrentUserJson()
//	for _, permission := range permissions {
//		switch permission {
//		case RequireLogin:
//			token := GetAccessToken(ctx)
//			if strutil.IsBlank(token) {
//				ctx.JSON(http.StatusUnauthorized, nil)
//				return false
//			}
//			userJson := GetUserJsonByToken(token)
//			if strutil.IsBlank(userJson) {
//				ctx.JSON(http.StatusUnauthorized, nil)
//				return false
//			}
//			setCurrentUserJson(userJson)
//			return true
//		}
//	}
//	return true
//}
