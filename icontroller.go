package ginplus

type IController interface {
	RequestMapping() string
}

// 编译校验：确保 RestController 完全实现了 IController 接口所有方法，如果有遗漏，编译报错
var _ IController = (*RestController)(nil)

// RestController 继承该控制器的结构体给浏览器返回JSON类型数据
// 示例代码：ctx.JSON(http.StatusOK, nil)
type RestController struct {
	Path string
}

func (rc RestController) RequestMapping() string {
	return rc.Path
}

// 编译校验：确保 Controller 完全实现了 IController 接口所有方法，如果有遗漏，编译报错
var _ IController = (*Controller)(nil)

// Controller 继承该控制器的结构体可以跳转html页面，给浏览器返回html类型响应
// 示例代码：ctx.HTML(http.StatusOK, "index.html", nil)
type Controller struct {
	Path string
}

func (rc Controller) RequestMapping() string {
	return rc.Path
}

// ModelAndView 使用Controller作为父类控制器时的返回值
// 也可以返回string，会被当作html路径解析
type ModelAndView struct {
	View       string `json:"view"`       // html路径
	Model      any    `json:"model"`      // 渲染的参数
	HttpStatus int    `json:"httpStatus"` // http响应码
}
