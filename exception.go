package ginplus

type bizError struct {
	Code int
	Msg  string
}

func (be *bizError) Error() string {
	return be.Msg
}

func NewBizErr(msg string) error {
	return NewBizErr2(FailCode, msg)
}

func NewBizErr2(code int, msg string) error {
	bizErr := new(bizError)
	bizErr.Code = code
	bizErr.Msg = msg
	return bizErr
}
