package ginplus

import (
	"gitee.com/hongzhaomin/hzm-common-go/strutil"
	"sync"
)

var (
	lock        = new(sync.Mutex)
	url2Mapping map[string]*handlerMapping
)

func getHandlerMapping(fullPath string) *handlerMapping {
	if strutil.IsBlank(fullPath) {
		return nil
	}
	return url2Mapping[fullPath]
}

func putHandlerMapping(fullPath string, handler *handlerMapping) {
	if strutil.IsNotBlank(fullPath) && handler != nil {
		lock.Lock()
		defer lock.Unlock()
		url2Mapping[fullPath] = handler
	}
}
