package ginplus

type Result[T any] struct {
	Code    int    `json:"code"`
	Msg     string `json:"msg"`
	Data    any    `json:"data"`
	Success bool   `json:"success"`
}

const (
	SucCode    = 200
	SucResult  = true
	FailCode   = -1
	FailResult = false
)

func Ok() Result[any] {
	rb := new(Result[any])
	rb.ok()
	return *rb
}

func Ok2[T any](data T) Result[T] {
	rb := new(Result[T])
	rb.ok()
	rb.Data = data
	return *rb
}

func Fail(msg string) Result[any] {
	rb := new(Result[any])
	rb.fail(msg)
	return *rb
}

func Fail2(code int, msg string) Result[any] {
	rb := new(Result[any])
	rb.fail(msg)
	rb.Code = code
	return *rb
}

func (rb *Result[T]) ok() *Result[T] {
	rb.Code = SucCode
	rb.Success = SucResult
	return rb
}

func (rb *Result[T]) fail(msg string) *Result[T] {
	rb.Code = FailCode
	rb.Success = FailResult
	rb.Msg = msg
	return rb
}
